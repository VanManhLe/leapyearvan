import unittest
from my_app.app import check_leap_year


class TestLeapYear(unittest.TestCase):
    def test_1(self):
        """
        Test that a year divisible by 400 is leap year
        """
        year = 2000
        result = check_leap_year(year)
        self.assertTrue(result)

    def test_2(self):
        """
        Test that a year divisible by 100 but not divisible by 400 is not a leap year
        """
        year_list = [1700, 1800, 1900]
        for year in year_list:
            result = check_leap_year(year)
            self.assertFalse(result)

    def test_3(self):
        """
        Test that a year divisible by 4 but not divisible by 100 is a leap year
        """
        year_list = [2008, 2012, 2016]
        for year in year_list:
            result = check_leap_year(year)
            self.assertTrue(result)

    def test_4(self):
        """
        Test that a year not divisible by 4 is not a leap year
        """
        year_list = [2017, 2018, 2019]
        for year in year_list:
            result = check_leap_year(year)
            self.assertFalse(result)

    def test_bad_type(self):
        """
        Test data type error
        """
        year = "year"
        with self.assertRaises(TypeError):
            result = check_leap_year(year)


if __name__ == '__main__':
    unittest.main()